package ru.skill_branch.mayorov.mvpauth.utils;


public class AppConfig {
    public static final String BASE_URL = "http://anyUrl.ru";
    public static long MAX_CONNECTION_TIMEOUT = 5000;
    public static long MAX_READ_TIMEOUT = 5000;
    public static long MAX_WRITE_TIMEOUT = 5000;
}
