package ru.skill_branch.mayorov.mvpauth.di.conponents;


import javax.inject.Singleton;

import dagger.Component;
import ru.skill_branch.mayorov.mvpauth.data.managers.DataManager;
import ru.skill_branch.mayorov.mvpauth.di.modules.LocalModule;
import ru.skill_branch.mayorov.mvpauth.di.modules.NetworkModule;

@Component(dependencies = AppComponent.class, modules = {NetworkModule.class, LocalModule.class})
@Singleton
public interface DataManagerComponent {
    void inject(DataManager dataManager);
}
