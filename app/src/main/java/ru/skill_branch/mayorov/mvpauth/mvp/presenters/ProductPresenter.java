package ru.skill_branch.mayorov.mvpauth.mvp.presenters;


import javax.inject.Inject;

import dagger.Provides;
import ru.skill_branch.mayorov.mvpauth.data.storage.dto.ProductDto;
import ru.skill_branch.mayorov.mvpauth.di.DaggerService;
import ru.skill_branch.mayorov.mvpauth.di.scopes.ProductScope;
import ru.skill_branch.mayorov.mvpauth.mvp.models.ProductModel;
import ru.skill_branch.mayorov.mvpauth.mvp.views.IProductView;

public class ProductPresenter extends AbstractPresenter<IProductView> implements IProductPresenter {

    @Inject
    ProductModel mProductModel;

    private ProductDto mProduct;


    public ProductPresenter(ProductDto product) {
        Component component = DaggerService.getComponent(Component.class);
        if(component == null){
            component = createDaggerComponent();
            DaggerService.registerComponent(Component.class,component);
        }
        component.inject(this);
        mProduct = product;
    }

    @Override
    public void initView() {
        if (getView() != null) {
            getView().showProductView(mProduct);
        }
    }

    @Override
    public void clickOnPlus() {
        mProduct.addProduct();
        mProductModel.updateProduct(mProduct);
        if(getView()!= null){
            getView().updateProductCountView(mProduct);
        }
    }

    @Override
    public void clickOnMinus() {
        if (mProduct.getCount() > 0) {
            mProduct.deleteProduct();
            mProductModel.updateProduct(mProduct);
            if (getView() != null) {
                getView().updateProductCountView(mProduct);
            }
        }
    }

    //region =================== DI ==========================
    private Component createDaggerComponent(){
        return DaggerProductPresenter_Component.builder()
                .module(new Module())
                .build();
    }

    @dagger.Module
    public class Module{
        @Provides
        @ProductScope
        ProductModel provideProductModel(){
            return new ProductModel();
        }
    }

    @dagger.Component(modules = Module.class)
    @ProductScope
    interface Component{
        void inject(ProductPresenter presenter);
    }
    //endregion
}
