package ru.skill_branch.mayorov.mvpauth.di;


import android.support.annotation.Nullable;
import android.util.Log;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class DaggerService {
    private static final String TAG = "DaggerService";
    private static Map<Class,Object> sComponentMap = new HashMap<>();

    @Nullable
    @SuppressWarnings("unchecked")
    public static  <T> T getComponent(Class<T> componentClass){
        Object component = sComponentMap.get(componentClass);
        return (T)component;
    }

    public static void registerComponent(Class componentClass, Object daggerComponent){
        sComponentMap.put(componentClass,daggerComponent);
    }

    public static void unregisterScope(Class<? extends Annotation> scopeAnnotation){
        Iterator<Map.Entry<Class,Object>> iterator = sComponentMap.entrySet().iterator();
        while(iterator.hasNext()){
            Map.Entry<Class,Object> entry = iterator.next();
            if(entry.getKey().isAnnotationPresent(scopeAnnotation)){
                Log.e(TAG, "unregisterScope: "+ entry.getKey().getName());
                iterator.remove();
            }
        }
    }


    public static <T> T createComponent(Class<T> componentClass, Object... dependencies){
        String componentClassName = componentClass.getName();
        String packageName = componentClass.getPackage().getName();
        String simpleName = componentClassName.substring(packageName.length() + 1);
        String generatedName = (packageName + ".Dagger" + simpleName).replace('$', '_');

        try {
            Class<?> generatedClass = Class.forName(generatedName);
            Object builder = generatedClass.getMethod("builder").invoke(null);

            for (Method method : builder.getClass().getDeclaredMethods()) {
                Class<?>[] params = method.getParameterTypes();
                if (params.length == 1) {
                    Class<?> dependencyClass = params[0];
                    for (Object dependency : dependencies) {
                        if (dependencyClass.isAssignableFrom(dependency.getClass())) {
                            method.invoke(builder, dependency);
                            break;
                        }
                    }
                }
            }
            //noinspection unchecked
            return (T) builder.getClass().getMethod("build").invoke(builder);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

}
