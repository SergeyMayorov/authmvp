package ru.skill_branch.mayorov.mvpauth.ui.activities;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.Provides;
import ru.skill_branch.mayorov.mvpauth.App;
import ru.skill_branch.mayorov.mvpauth.BuildConfig;
import ru.skill_branch.mayorov.mvpauth.R;
import ru.skill_branch.mayorov.mvpauth.data.managers.DataManager;
import ru.skill_branch.mayorov.mvpauth.di.DaggerService;
import ru.skill_branch.mayorov.mvpauth.di.conponents.DaggerPicassoComponent;
import ru.skill_branch.mayorov.mvpauth.di.conponents.PicassoComponent;
import ru.skill_branch.mayorov.mvpauth.di.modules.PicassoCacheModule;
import ru.skill_branch.mayorov.mvpauth.di.scopes.RootScope;
import ru.skill_branch.mayorov.mvpauth.mvp.presenters.AuthPresenter;
import ru.skill_branch.mayorov.mvpauth.mvp.presenters.RootPresenter;
import ru.skill_branch.mayorov.mvpauth.mvp.views.IRootView;
import ru.skill_branch.mayorov.mvpauth.mvp.views.IView;
import ru.skill_branch.mayorov.mvpauth.ui.fragments.AccountFragment;
import ru.skill_branch.mayorov.mvpauth.ui.fragments.AuthFragment;
import ru.skill_branch.mayorov.mvpauth.ui.fragments.CatalogFragment;
import ru.skill_branch.mayorov.mvpauth.utils.DialogHelper;
import ru.skill_branch.mayorov.mvpauth.utils.RoundedAvatarDrawable;


public class RootActivity extends AppCompatActivity implements IRootView,
        NavigationView.OnNavigationItemSelectedListener,
        DialogInterface.OnClickListener {
    private final static String TAG = "RootActivity";
    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawer;

    @BindView(R.id.nav_view)
    NavigationView mNavigationView;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.coordinator_container)
    CoordinatorLayout mCoordinatorLayout;

    @BindView(R.id.fragment_container)
    FrameLayout mFragmentContainer;

    FragmentManager mFragmentManager;

    @Inject
    RootPresenter mRootPresenter;

    @Inject
    Picasso mPicasso;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);
        ButterKnife.bind(this);

        Component component = DaggerService.getComponent(Component.class);
        if (component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(Component.class, component);
        }
        component.inject(this);

        initToolbar();
        initDrawer();
        mRootPresenter.takeView(this);
        mRootPresenter.initView();
        //TODO initView

        mFragmentManager = getSupportFragmentManager();
        if (savedInstanceState == null) {
            mFragmentManager.beginTransaction().replace(R.id.fragment_container, new CatalogFragment())
                    .commit();
        }
    }

    @Override
    protected void onDestroy() {
        mRootPresenter.dropView();
        super.onDestroy();
    }

    private void initDrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawer, mToolbar, R.string.open_drawer, R.string.close_drawer);
        mDrawer.setDrawerListener(toggle);


        toggle.syncState();
        mNavigationView.setNavigationItemSelectedListener(this);

        ImageView avatar = (ImageView)mNavigationView.getHeaderView(0).findViewById(R.id.drawer_user_avatar_iv);

        mPicasso.with(this)
                .load(R.drawable.user_avatar)
                .fit()
                .centerCrop()
                .transform(new RoundedAvatarDrawable())
                .placeholder(R.drawable.user_avatar)
                .into(avatar);


    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
    }

    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START);
        } else if (mFragmentManager.getBackStackEntryCount() == 0) {
            DialogHelper.createDialog(this, "Exit", "Are you sure you want to exit?", this).show();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;

        switch (item.getItemId()) {
            case R.id.nav_account:
                fragment = new AccountFragment();
                break;
            case R.id.nav_catalog:
                fragment = new CatalogFragment();
                break;
            case R.id.nav_favorite:
                break;
            case R.id.nav_orders:
                break;
            case R.id.nav_notification:
                break;
        }
        if (fragment != null) {
            mFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .addToBackStack(null)
                    .commit();
        }
        mDrawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void showAuthScreen() {
        mFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, new AuthFragment())
                .addToBackStack(null)
                .commit();
    }

    //region ================ IRootView ================
    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable e) {
        if (BuildConfig.DEBUG) {
            showMessage(e.getMessage());
            e.printStackTrace();
        } else {
            showMessage(getString(R.string.error_message));
        }
    }


    @Override
    public void showLoad() {
//        if (mProgressDialog == null) {
//            mProgressDialog = new ProgressDialog(this, R.style.custom_dialog);
//            mProgressDialog.setCancelable(false);
//            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        }
//        mProgressDialog.show();
//        mProgressDialog.setContentView(R.layout.progress_splash);
    }

    @Override
    public void hideLoad() {
//        if (mProgressDialog != null) {
//            if (mProgressDialog.isShowing()) {
//                mProgressDialog.dismiss();
//            }
//        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (which == Dialog.BUTTON_POSITIVE) {
            super.onBackPressed();
        } else if (which == Dialog.BUTTON_NEGATIVE) {
            dialog.dismiss();
        }
    }
    //endregion


    //region ==================== DI ========================
    private Component createDaggerComponent() {
        PicassoComponent picassoComponent = DaggerService.getComponent(PicassoComponent.class);
        if (picassoComponent == null) {
            picassoComponent = DaggerService.createComponent(PicassoComponent.class,
                    App.getAppComponent(),
                    new PicassoCacheModule());
//            picassoComponent = DaggerPicassoComponent.builder()
//                    .appComponent(App.getAppComponent())
//                    .picassoCacheModule(new PicassoCacheModule())
//                    .build();
            DaggerService.registerComponent(PicassoComponent.class, picassoComponent);
        }

        return DaggerService.createComponent(Component.class, picassoComponent,new Module() );
//                DaggerRootActivity_Component.builder()
//                .module(new Module())
//                .picassoComponent(picassoComponent)
//                .build();
    }

    @dagger.Module
    public class Module {
        @Provides
        @RootScope
        RootPresenter provideRootPresenter() {
            return new RootPresenter();
        }
    }

    @dagger.Component(dependencies = PicassoComponent.class, modules = Module.class)
    @RootScope
    public interface Component {
        void inject(RootActivity activity);

        RootPresenter getRootPresenter();
    }


    //endregion

}