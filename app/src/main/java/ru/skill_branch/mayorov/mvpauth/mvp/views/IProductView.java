package ru.skill_branch.mayorov.mvpauth.mvp.views;

import ru.skill_branch.mayorov.mvpauth.data.storage.dto.ProductDto;

/**
 * Created by Sergey on 29.10.2016.
 */

public interface IProductView extends IView {
    void showProductView(ProductDto product);

    void updateProductCountView(ProductDto product);
}
