package ru.skill_branch.mayorov.mvpauth.mvp.presenters;

import android.support.annotation.Nullable;

import ru.skill_branch.mayorov.mvpauth.mvp.views.IAuthView;

/**
 * Created by Sergey on 22.10.2016.
 */
public interface IAuthPresenter {
    void takeView(IAuthView authView);
    void dropView();
    void initView();

    @Nullable
    IAuthView getView();

    void clickOnLogin();
    void clickOnFb();
    void clickOnVk();
    void clickOnTwitter();
    void clickOnShowCatalog();

    boolean checkUserAuth();

}
