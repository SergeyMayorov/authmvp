package ru.skill_branch.mayorov.mvpauth.mvp.models;

import java.util.List;

import ru.skill_branch.mayorov.mvpauth.data.managers.DataManager;
import ru.skill_branch.mayorov.mvpauth.data.storage.dto.ProductDto;



public class CatalogModel extends AbstractModel {
    public CatalogModel() {
    }

    public List<ProductDto> getProductList(){
        return mDataManager.getProductList();
    }

    public boolean isUserAuth(){
        return mDataManager.isAuthUser();
    }
}
