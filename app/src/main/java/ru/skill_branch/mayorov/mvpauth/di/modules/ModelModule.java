package ru.skill_branch.mayorov.mvpauth.di.modules;


import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.skill_branch.mayorov.mvpauth.data.managers.DataManager;

@Module
public class ModelModule {
    @Provides
    @Singleton
    DataManager provideDataManager(){
        return new DataManager();
    }
}
