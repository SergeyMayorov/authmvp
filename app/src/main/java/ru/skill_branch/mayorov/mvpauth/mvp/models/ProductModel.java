package ru.skill_branch.mayorov.mvpauth.mvp.models;


import ru.skill_branch.mayorov.mvpauth.data.storage.dto.ProductDto;


public class ProductModel extends AbstractModel {
    public ProductDto getProductById(int productId){
        //TODO : get product fron datamanager
        return mDataManager.getProductById(productId);
    }

    public void updateProduct(ProductDto product){
        mDataManager.updateProduct(product);
    }
}
