package ru.skill_branch.mayorov.mvpauth.di.conponents;

import android.content.Context;

import dagger.Component;
import ru.skill_branch.mayorov.mvpauth.di.modules.AppModule;

@Component(modules = AppModule.class)
public interface AppComponent {
    Context getContext();
}
