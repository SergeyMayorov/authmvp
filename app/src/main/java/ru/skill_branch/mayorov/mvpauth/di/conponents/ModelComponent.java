package ru.skill_branch.mayorov.mvpauth.di.conponents;

import javax.inject.Singleton;

import dagger.Component;
import ru.skill_branch.mayorov.mvpauth.di.modules.ModelModule;
import ru.skill_branch.mayorov.mvpauth.mvp.models.AbstractModel;

@Component(modules = ModelModule.class)
@Singleton
public interface ModelComponent {
    void inject(AbstractModel abstractModel);
}
