package ru.skill_branch.mayorov.mvpauth.di.conponents;

import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Component;
import ru.skill_branch.mayorov.mvpauth.di.modules.PicassoCacheModule;

@Component(dependencies = AppComponent.class, modules = PicassoCacheModule.class)
@Singleton
public interface PicassoComponent {
    Picasso getPicasso();
}
