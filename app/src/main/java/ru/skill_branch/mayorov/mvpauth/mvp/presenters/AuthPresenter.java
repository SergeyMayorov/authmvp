package ru.skill_branch.mayorov.mvpauth.mvp.presenters;


import android.os.Handler;

import android.util.Log;

import javax.inject.Inject;

import dagger.Provides;
import ru.skill_branch.mayorov.mvpauth.di.DaggerService;
import ru.skill_branch.mayorov.mvpauth.di.scopes.AuthScope;
import ru.skill_branch.mayorov.mvpauth.mvp.models.AuthModel;
import ru.skill_branch.mayorov.mvpauth.mvp.views.IAuthView;
import ru.skill_branch.mayorov.mvpauth.ui.custom_views.AuthPanel;


public class AuthPresenter extends AbstractPresenter<IAuthView> implements IAuthPresenter {
    private static final String TAG = "AuthPresenter";

    @Inject
    AuthModel mAuthModel;

    public AuthPresenter() {
        Component component = DaggerService.getComponent(Component.class);
        if(component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(Component.class,component);
        }
        component.inject(this);
        Log.e(TAG, "AuthPresenter: inject complete");
    }


    @Override
    public void initView() {
        if (getView() != null) {
            if (checkUserAuth()) {
                getView().hideLoginBtn();
            } else {
                getView().showLoginBtn();
            }
        }
    }


    @Override
    public void clickOnLogin() {
        if (getView() != null && getView().getAuthPanel() != null) {
            if (getView().getAuthPanel().isIdle()) {
                getView().getAuthPanel().setCustomState(AuthPanel.LOGIN_STATE);
            } else {
                boolean isEmailValid = getView().getAuthPanel().checkEmailValidation();
                boolean isPasswordValid = getView().getAuthPanel().checkPasswordValidation();

                if (isPasswordValid && isEmailValid) {
                    //if(true){
                    getView().showLoad();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mAuthModel.loginUser(getView().getAuthPanel().getUserEmail(), getView().getAuthPanel().getUserPassword());
                            getView().showMessage("request for user auth");
                            getView().getAuthPanel().setCustomState(AuthPanel.IDLE_STATE);
                            getView().hideLoginBtn();
                            getView().hideLoad();
                        }
                    }, 3000);
                }
            }
        }
    }


    @Override
    public void clickOnFb() {
        if (getView() != null) {
            getView().showMessage("clickOnFb");
        }
    }

    @Override
    public void clickOnVk() {
        if (getView() != null) {
            getView().showMessage("clickOnVk");
        }
    }

    @Override
    public void clickOnTwitter() {
        if (getView() != null) {
            getView().showMessage("clickOnTwitter");
        }
    }

    @Override
    public void clickOnShowCatalog() {
        if (getView() != null) {
            getView().showMessage("Показать каталог");
            // TODO: 25.10.2016 if update data complete start catalog Screen
            getView().showCatalogScreen();
        }
    }

    @Override
    public boolean checkUserAuth() {
        return mAuthModel.isAuthUser();
    }

    //region ================== DI =======================
    @dagger.Module
    public class Module {
        @AuthScope
        @Provides
        AuthModel provideAuthModel() {
            return new AuthModel();
        }
    }

    @AuthScope
    @dagger.Component(modules = Module.class)
    interface Component {
        void inject(AuthPresenter presenter);
    }

    private Component createDaggerComponent() {
        return DaggerAuthPresenter_Component.builder()
                .module(new Module())
                .build();
    }
    //endregion
}
