package ru.skill_branch.mayorov.mvpauth.mvp.views;


import java.util.List;

import ru.skill_branch.mayorov.mvpauth.data.storage.dto.ProductDto;

public interface ICatalogView extends IView{
    //void showAddToCartMessage(ProductDto productDto);
    void showCatalogView(List<ProductDto> productList);
    void showAuthScreen();
    void updateProductCounter();
}
