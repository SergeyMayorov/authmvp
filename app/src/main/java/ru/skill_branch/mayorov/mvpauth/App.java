package ru.skill_branch.mayorov.mvpauth;

import android.app.Application;

import ru.skill_branch.mayorov.mvpauth.di.DaggerService;
import ru.skill_branch.mayorov.mvpauth.di.conponents.AppComponent;
import ru.skill_branch.mayorov.mvpauth.di.conponents.DaggerAppComponent;
import ru.skill_branch.mayorov.mvpauth.di.modules.AppModule;


public class App extends Application {

    private static AppComponent sAppComponent;

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        createComponent();
    }

    private void createComponent() {
        sAppComponent = DaggerService.createComponent(AppComponent.class, new AppModule(getApplicationContext()));
//                DaggerAppComponent.builder()
//                .appModule(new AppModule(getApplicationContext()))
//                .build();
    }

}
