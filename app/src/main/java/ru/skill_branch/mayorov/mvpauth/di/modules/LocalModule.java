package ru.skill_branch.mayorov.mvpauth.di.modules;


import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.skill_branch.mayorov.mvpauth.data.managers.PreferencesManager;

@Module
public class LocalModule {

    @Provides
    @Singleton
    PreferencesManager providePreferencesManager(Context context){
        return new PreferencesManager(context);
    }

}
