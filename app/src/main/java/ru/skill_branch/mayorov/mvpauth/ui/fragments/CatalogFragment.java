package ru.skill_branch.mayorov.mvpauth.ui.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.Component;
import dagger.Provides;
import ru.skill_branch.mayorov.mvpauth.R;
import ru.skill_branch.mayorov.mvpauth.data.storage.dto.ProductDto;
import ru.skill_branch.mayorov.mvpauth.di.DaggerService;
import ru.skill_branch.mayorov.mvpauth.di.scopes.CatalogScope;
import ru.skill_branch.mayorov.mvpauth.mvp.presenters.CatalogPresenter;
import ru.skill_branch.mayorov.mvpauth.mvp.views.ICatalogView;
import ru.skill_branch.mayorov.mvpauth.ui.activities.RootActivity;
import ru.skill_branch.mayorov.mvpauth.ui.fragments.adapters.CatalogAdapter;

public class CatalogFragment extends Fragment implements ICatalogView, View.OnClickListener {
    @Inject
    CatalogPresenter mPresenter;

    @BindView(R.id.add_to_card_btn)
    Button mAddToCardBtn;

    @BindView(R.id.product_pager)
    ViewPager mProductPager;

    public CatalogFragment() {
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_catalog, container, false);
        ButterKnife.bind(this, view);

        Component component = DaggerService.getComponent(Component.class);
        if(component == null){
            component = DaggerService.createComponent(Component.class, new Module());//createDaggerComponent();
            DaggerService.registerComponent(Component.class,component);
        }
        component.inject(this);

        mPresenter.takeView(this);
        mPresenter.initView();
        mAddToCardBtn.setOnClickListener(this);
        return view;
    }

    @Override
    public void onDestroyView() {
        mProductPager.setAdapter(null);
        mPresenter.dropView();
        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.add_to_card_btn) {
            mPresenter.clickOnBuyButton(mProductPager.getCurrentItem());
        }
    }

    //region ============= ICatalogView ===============


    @Override
    public void showCatalogView(List<ProductDto> productList) {
        CatalogAdapter adapter = new CatalogAdapter(getChildFragmentManager());

        for (ProductDto product : productList) {
            adapter.addItem(product);
        }
        mProductPager.setAdapter(adapter);
    }

    @Override
    public void showAuthScreen() {
        //TODO showAuthScreen
        //getRootActivity().showAuthScreen();
    }

    @Override
    public void updateProductCounter() {
        //todo update count product on cart icon
    }
    //endregion



    //region ========================= DI ============================

//    private Component createDaggerComponent(){
//        return DaggerCatalogFragment_Component.builder()
//                .module(new Module())
//                .build();
//    }

    @dagger.Module
    public class Module{
        @Provides
        @CatalogScope
        CatalogPresenter provideCatalogPresenter(){
            return new CatalogPresenter();
        }
    }

    @dagger.Component(modules = Module.class)
    @CatalogScope
    interface Component{
        void inject(CatalogFragment fragment);
    }
    //endregion

}
