package ru.skill_branch.mayorov.mvpauth.mvp.presenters;


public interface ICatalogPresenter {
    void clickOnBuyButton(int position);
    boolean checkUserAuth();
}
