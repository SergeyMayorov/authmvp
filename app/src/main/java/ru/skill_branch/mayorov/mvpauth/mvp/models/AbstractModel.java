package ru.skill_branch.mayorov.mvpauth.mvp.models;


import javax.inject.Inject;

import ru.skill_branch.mayorov.mvpauth.data.managers.DataManager;
import ru.skill_branch.mayorov.mvpauth.di.DaggerService;
import ru.skill_branch.mayorov.mvpauth.di.conponents.DaggerModelComponent;
import ru.skill_branch.mayorov.mvpauth.di.conponents.ModelComponent;
import ru.skill_branch.mayorov.mvpauth.di.modules.ModelModule;

public abstract class AbstractModel {
    @Inject
    DataManager mDataManager;

    public AbstractModel() {
        ModelComponent component = DaggerService.getComponent(ModelComponent.class);
        if (component == null) {
            component = DaggerService.createComponent(ModelComponent.class, new ModelModule());//createDaggerComponent();
            DaggerService.registerComponent(ModelComponent.class,component);
        }
        component.inject(this);
    }

    private ModelComponent createDaggerComponent() {
        return DaggerModelComponent.builder()
                .modelModule(new ModelModule())
                .build();
    }
}
