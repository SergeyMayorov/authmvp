package ru.skill_branch.mayorov.mvpauth.ui.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.media.session.MediaControllerCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.Provides;
import ru.skill_branch.mayorov.mvpauth.App;
import ru.skill_branch.mayorov.mvpauth.R;
import ru.skill_branch.mayorov.mvpauth.data.storage.dto.ProductDto;
import ru.skill_branch.mayorov.mvpauth.di.DaggerService;
import ru.skill_branch.mayorov.mvpauth.di.conponents.DaggerPicassoComponent;
import ru.skill_branch.mayorov.mvpauth.di.conponents.PicassoComponent;
import ru.skill_branch.mayorov.mvpauth.di.modules.PicassoCacheModule;
import ru.skill_branch.mayorov.mvpauth.di.scopes.ProductScope;
import ru.skill_branch.mayorov.mvpauth.mvp.presenters.ProductPresenter;
import ru.skill_branch.mayorov.mvpauth.mvp.views.IProductView;
import ru.skill_branch.mayorov.mvpauth.ui.activities.RootActivity;
import ru.skill_branch.mayorov.mvpauth.ui.custom_views.AspectRatioImageView;

public class ProductFragment extends Fragment implements IProductView, View.OnClickListener {
    private static final String TAG = "ProductFragment";

    @BindView(R.id.product_name_txt)
    TextView mProductNameTxt;

    @BindView(R.id.product_description_txt)
    TextView mProductDescriptionTxt;

    @BindView(R.id.product_image)
    AspectRatioImageView mProductImage;

    @BindView(R.id.product_count_txt)
    TextView mProductCountTxt;

    @BindView(R.id.product_price_txt)
    TextView mProductPriceTxt;

    @BindView(R.id.plus_btn)
    ImageButton mPlusBtn;

    @BindView(R.id.minus_btn)
    ImageButton mMinusBtn;

    @Inject
    Picasso mPicasso;

    @Inject
    ProductPresenter mPresenter;


    public ProductFragment() {
    }


    public static ProductFragment newInstance(ProductDto product) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("PRODUCT", product);
        ProductFragment fragment = new ProductFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    private void readBundle(Bundle bundle) {
        if (bundle != null) {
            ProductDto product = bundle.getParcelable("PRODUCT");
            Component component = createDaggerComponent(product);
            component.inject(this);

            //TODO fix recreate component
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product, container, false);
        ButterKnife.bind(this, view);
        readBundle(getArguments());
        mPresenter.takeView(this);
        mPresenter.initView();
        mPlusBtn.setOnClickListener(this);
        mMinusBtn.setOnClickListener(this);
        return view;
    }

    @Override
    public void onDestroyView() {
        mPresenter.dropView();
        super.onDestroyView();
    }

    //region ============== IProductView ================
    @Override
    public void showProductView(final ProductDto product) {
        mProductNameTxt.setText(product.getProductName());
        mProductDescriptionTxt.setText(product.getDescription());
        mProductCountTxt.setText(String.valueOf(product.getCount()));
        if (product.getCount() > 0) {
            mProductPriceTxt.setText(String.valueOf(product.getCount() * product.getPrice() + ".-"));
        } else {
            mProductPriceTxt.setText(String.valueOf(product.getPrice() + ".-"));
        }


        mPicasso
                .load(product.getImageUrl())
                .networkPolicy(NetworkPolicy.OFFLINE)
                .fit()
                .centerCrop()
                .into(mProductImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        Log.d(TAG, "onSuccess: load from cache");
                    }

                    @Override
                    public void onError() {
                        mPicasso
                                .load(product.getImageUrl())
                                .fit()
                                .centerCrop()
                                .into(mProductImage);
                    }
                });

    }


    @Override
    public void updateProductCountView(ProductDto product) {
        mProductCountTxt.setText(String.valueOf(product.getCount()));
        if (product.getCount() > 0) {
            mProductPriceTxt.setText(String.valueOf(product.getCount() * product.getPrice() + ".-"));
        }
    }


    //endregion

    private RootActivity getRootActivity() {
        return (RootActivity) getActivity();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.plus_btn:
                mPresenter.clickOnPlus();
                break;

            case R.id.minus_btn:
                mPresenter.clickOnMinus();
                break;
        }
    }


    //region =================== DI ========================
    private Component createDaggerComponent(ProductDto product) {
        PicassoComponent picassoComponent = DaggerService.getComponent(PicassoComponent.class);
        if (picassoComponent == null) {
            picassoComponent = DaggerService.createComponent(PicassoComponent.class,
                    App.getAppComponent(),
                    new PicassoCacheModule());

//            picassoComponent = DaggerPicassoComponent.builder()
//                    .appComponent(App.getAppComponent())
//                    .picassoCacheModule(new PicassoCacheModule())
//                    .build();

            DaggerService.registerComponent(PicassoComponent.class, picassoComponent);
        }
        return DaggerService.createComponent(Component.class, picassoComponent,new Module(product));


//        return DaggerProductFragment_Component.builder()
//                .picassoComponent(picassoComponent)
//                .module(new Module(product))
//                .build();
    }


    @dagger.Module
    public class Module {
        ProductDto mProductDto;

        public Module(ProductDto productDto) {
            mProductDto = productDto;
        }

        @Provides
        @ProductScope
        ProductPresenter provideProductPresenter() {
            return new ProductPresenter(mProductDto);
        }

    }

    @dagger.Component(dependencies = PicassoComponent.class, modules = Module.class)
    @ProductScope
    interface Component {
        void inject(ProductFragment fragment);
    }
    //endregion
}