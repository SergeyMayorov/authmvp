package ru.skill_branch.mayorov.mvpauth.mvp.views;

import android.support.annotation.Nullable;

import ru.skill_branch.mayorov.mvpauth.mvp.presenters.IAuthPresenter;
import ru.skill_branch.mayorov.mvpauth.ui.custom_views.AuthPanel;


public interface IAuthView extends IRootView {

    IAuthPresenter getPresenter();

    void showLoginBtn();
    void hideLoginBtn();

    @Nullable
    AuthPanel getAuthPanel();

    void showCatalogScreen();
}
