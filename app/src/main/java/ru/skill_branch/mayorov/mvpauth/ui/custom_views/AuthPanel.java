package ru.skill_branch.mayorov.mvpauth.ui.custom_views;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.skill_branch.mayorov.mvpauth.R;
import ru.skill_branch.mayorov.mvpauth.utils.Validator;


public class AuthPanel extends LinearLayout {

    public static final int LOGIN_STATE = 0;
    public static final int IDLE_STATE = 1;
    private int mCustomState = 1;


    @BindView(R.id.auth_card)
    CardView mAuthCard;

    @BindView(R.id.login_email_wrap)
    TextInputLayout mEmailWrapper;

    @BindView(R.id.login_email_et)
    EditText mEmailEt;

    @BindView(R.id.login_password_wrap)
    TextInputLayout mPasswordWrapper;

    @BindView(R.id.login_password_et)
    EditText mPasswordEt;

    @BindView(R.id.login_btn)
    Button mLoginBtn;

    @BindView(R.id.show_catalog_btn)
    Button mShowCatalogBtn;

    public AuthPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
        showViewFromState();
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        SavedState savedState = new SavedState(superState);
        savedState.state = mCustomState;
        return savedState;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());
        setCustomState(savedState.state);
    }

    public void setCustomState(int state) {
        mCustomState = state;
        showViewFromState();
    }

    private void showLoginState() {
        mAuthCard.setVisibility(VISIBLE);
        mShowCatalogBtn.setVisibility(GONE);
    }

    private void showIdleState() {
        //collapse(mAuthCard);

        mAuthCard.setVisibility(GONE);
        mShowCatalogBtn.setVisibility(VISIBLE);
    }

//    private void collapse(final View view){
//        final int initialHeight = view.getMeasuredHeight();
//
//        Animation a = new Animation()
//        {
//            @Override
//            protected void applyTransformation(float interpolatedTime, Transformation t) {
//                if(interpolatedTime == 1){
//                    view.setVisibility(View.GONE);
//                }else{
//                    view.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
//                    view.requestLayout();
//                }
//            }
//
//            @Override
//            public boolean willChangeBounds() {
//                return true;
//            }
//        };
//
//        // 1dp/ms
//        a.setDuration((int)(initialHeight / view.getContext().getResources().getDisplayMetrics().density));
//        view.startAnimation(a);
//    }

    private void showViewFromState() {
        if (mCustomState == LOGIN_STATE) {
            showLoginState();
        } else {
            showIdleState();
        }
    }

    public String getUserEmail() {
        return String.valueOf(mEmailEt.getText());
    }

    public String getUserPassword() {
        return String.valueOf(mPasswordEt.getText());
    }

    public boolean checkEmailValidation(){
        if(Validator.isValidEmail(getUserEmail())){
            mEmailWrapper.setErrorEnabled(false);
            return true;
        }
        else {
            mEmailWrapper.setError("Not a valid email address!");
            return false;
        }
    }

    public boolean checkPasswordValidation(){
        if(Validator.isValidPassword(getUserPassword())){
            mPasswordWrapper.setErrorEnabled(false);
            return true;
        }
        else {
            mPasswordWrapper.setError("Not a valid password!");
            return false;
        }
    }

    public boolean isIdle(){
        return mCustomState == IDLE_STATE;
    }

    static class SavedState extends BaseSavedState {
        private int state;

        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {

            @Override
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            @Override
            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };

        public SavedState(Parcelable superState) {
            super(superState);
        }

        private SavedState(Parcel in) {
            super(in);
            state = in.readInt();
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeInt(state);
        }
    }
}
