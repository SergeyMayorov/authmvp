package ru.skill_branch.mayorov.mvpauth.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.skill_branch.mayorov.mvpauth.R;

public class AuthFragment extends Fragment implements View.OnClickListener {

    @BindView(R.id.login_email_et)
    EditText mLoginTxt;

    @BindView(R.id.login_password_et)
    EditText mLoginPw;

    @BindView(R.id.login_btn)
    Button mLoginButton;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_auth, container, false);
        ButterKnife.bind(this, view);
        mLoginButton.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {

    }
}
