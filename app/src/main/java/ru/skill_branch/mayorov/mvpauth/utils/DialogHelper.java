package ru.skill_branch.mayorov.mvpauth.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

public class DialogHelper {

    public static Dialog createDialog(Context context,
                                      String title,
                                      String message,
                                      final DialogInterface.OnClickListener listener){
        return new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Yes", listener)
                .setNegativeButton("No", listener)
                .create();
    }
}
