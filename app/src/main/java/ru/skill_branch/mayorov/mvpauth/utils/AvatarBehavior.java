package ru.skill_branch.mayorov.mvpauth.utils;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import ru.skill_branch.mayorov.mvpauth.ui.custom_views.AspectRatioImageView;

public class AvatarBehavior extends CoordinatorLayout.Behavior<ImageView> {
    private static final String TAG = "AvatarBehavior";


    public AvatarBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, ImageView child, View dependency) {
        return dependency instanceof AspectRatioImageView;
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, ImageView child, View dependency) {
        Log.d(TAG, "TranslationY: " + dependency.getTranslationY() + "; Height: "+dependency.getHeight());


        return super.onDependentViewChanged(parent, child, dependency);
    }
}
