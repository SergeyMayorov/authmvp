package ru.skill_branch.mayorov.mvpauth.utils;

import android.text.TextUtils;

/**
 * Created by Sergey on 22.10.2016.
 */

public class Validator {
    public static boolean isValidEmail(String target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static boolean isValidPassword(String password){
        return password.length() >= 8;
    }
}
