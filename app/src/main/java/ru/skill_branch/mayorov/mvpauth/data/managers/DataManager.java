package ru.skill_branch.mayorov.mvpauth.data.managers;

import android.content.Context;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import ru.skill_branch.mayorov.mvpauth.data.network.PicassoCache;
import ru.skill_branch.mayorov.mvpauth.data.network.RestService;
import ru.skill_branch.mayorov.mvpauth.data.storage.dto.ProductDto;
import ru.skill_branch.mayorov.mvpauth.App;
import ru.skill_branch.mayorov.mvpauth.di.DaggerService;
import ru.skill_branch.mayorov.mvpauth.di.conponents.DaggerDataManagerComponent;
import ru.skill_branch.mayorov.mvpauth.di.conponents.DataManagerComponent;
import ru.skill_branch.mayorov.mvpauth.di.modules.LocalModule;
import ru.skill_branch.mayorov.mvpauth.di.modules.NetworkModule;


public class DataManager {
    private Context mContext;

    @Inject
    PreferencesManager mPreferencesManager;
    @Inject
    RestService mRestService;

    private List<ProductDto> mMockProductList;


    public DataManager() {
        DataManagerComponent component = DaggerService.getComponent(DataManagerComponent.class);
        if(component == null){
            component = DaggerService.createComponent(DataManagerComponent.class,App.getAppComponent(),new LocalModule(),new NetworkModule());
//            component = DaggerDataManagerComponent.builder()
//                    .appComponent(App.getAppComponent())
//                    .localModule(new LocalModule())
//                    .networkModule(new NetworkModule())
//                    .build();
            DaggerService.registerComponent(DataManagerComponent.class,component);
        }
        component.inject(this);

        generateMockData();
    }



    public PreferencesManager getPreferencesManager() {
        return mPreferencesManager;
    }

    public ProductDto getProductById(int productId) {
        return mMockProductList.get(productId);
    }

    public void updateProduct(ProductDto product) {
    }

    public List<ProductDto> getProductList(){
       return mMockProductList;
    }

    private void generateMockData() {
        mMockProductList = new ArrayList<>();
        mMockProductList.add(new ProductDto(1, "Apple MacBook Air 13 Early 2016", "https://mdata.yandex.net/i?path=b0421213344_img_id783448203563828837.jpeg&size=9", "смартфон, Android 6.0", 26140, 1));
        mMockProductList.add(new ProductDto(2, "test 2", "https://mdata.yandex.net/i?path=b0407152326_img_id8945297374671695089.jpeg&size=9", "description1 description1 description1 description1", 102, 1));
        mMockProductList.add(new ProductDto(3, "test 3", "https://mdata.yandex.net/i?path=b0126181358_img_id5955660308062690792.jpeg&size=9", "description1 description1 description1 description1", 13, 1));
        mMockProductList.add(new ProductDto(4, "test 4", "https://mdata.yandex.net/i?path=b0604201304_img_id5054231374889377812.jpg&size=9", "description1 description1 description1 description1", 140, 1));
        mMockProductList.add(new ProductDto(5, "test 5", "https://yastatic.net/market-export/_/i/desktop/big-box.png", "description1 description1 description1 description1", 10, 1));
        mMockProductList.add(new ProductDto(6, "test 6", "https://mdata.yandex.net/i?path=b0328010331_img_id4892713239355658297.jpeg&size=9", "description1 description1 description1 description1", 600, 1));
        mMockProductList.add(new ProductDto(7, "test 7", "https://mdata.yandex.net/i?path=b1121040655_img_id4776793742793746786.jpeg&size=9", "description1 description1 description1 description1", 700, 1));
        mMockProductList.add(new ProductDto(8, "test 8", "https://mdata.yandex.net/i?path=b1121040655_img_id4776793742793746786.jpeg&size=9", "description1 description1 description1 description1", 130, 1));
        mMockProductList.add(new ProductDto(9, "test 9", "https://mdata.yandex.net/i?path=b1121040655_img_id4776793742793746786.jpeg&size=9", "description1 description1 description1 description1", 110, 1));

    }

    public boolean isAuthUser() {
        //todo check user auth token in shared preferences
        return true;
    }

}
