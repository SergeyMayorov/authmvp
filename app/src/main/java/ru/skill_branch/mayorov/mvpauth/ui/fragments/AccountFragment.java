package ru.skill_branch.mayorov.mvpauth.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.Provides;
import ru.skill_branch.mayorov.mvpauth.App;
import ru.skill_branch.mayorov.mvpauth.R;
import ru.skill_branch.mayorov.mvpauth.data.storage.dto.ProductDto;
import ru.skill_branch.mayorov.mvpauth.di.DaggerService;
import ru.skill_branch.mayorov.mvpauth.di.conponents.PicassoComponent;
import ru.skill_branch.mayorov.mvpauth.di.modules.PicassoCacheModule;
import ru.skill_branch.mayorov.mvpauth.di.scopes.AccountScope;
import ru.skill_branch.mayorov.mvpauth.di.scopes.ProductScope;
import ru.skill_branch.mayorov.mvpauth.mvp.presenters.AccountPresenter;
import ru.skill_branch.mayorov.mvpauth.mvp.presenters.ProductPresenter;
import ru.skill_branch.mayorov.mvpauth.mvp.views.IAccountView;
import ru.skill_branch.mayorov.mvpauth.ui.activities.RootActivity;
import ru.skill_branch.mayorov.mvpauth.utils.RoundedAvatarDrawable;

public class AccountFragment extends Fragment implements IAccountView {

    @BindView(R.id.avatar_img)
    ImageView avatarImage;

    @Inject
    Picasso mPicasso;

    public AccountFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        AccountFragment.Component component = createDaggerComponent();
        component.inject(this);

        View view = inflater.inflate(R.layout.fragment_account, container, false);
        ButterKnife.bind(this,view);

        mPicasso.with(getRootActivity())
                .load(R.drawable.user_avatar)
                .fit()
                .centerCrop()
                .transform(new RoundedAvatarDrawable())
                //.placeholder(R.drawable.user_avatar)
                .into(avatarImage);

        return view;
    }

    private RootActivity getRootActivity() {
        return (RootActivity) getActivity();
    }

    //region =================== DI ========================
    private AccountFragment.Component createDaggerComponent() {
        PicassoComponent picassoComponent = DaggerService.getComponent(PicassoComponent.class);
        if (picassoComponent == null) {
            picassoComponent = DaggerService.createComponent(PicassoComponent.class,
                    App.getAppComponent(),
                    new PicassoCacheModule());
            DaggerService.registerComponent(PicassoComponent.class, picassoComponent);
        }
        return DaggerService.createComponent(AccountFragment.Component.class, picassoComponent, new Module());
    }


    @dagger.Module
    public class Module {
        @Provides
        @AccountScope
        AccountPresenter provideProductPresenter() {
            return new AccountPresenter();
        }

    }

    @dagger.Component(dependencies = PicassoComponent.class, modules = AccountFragment.Module.class)
    @AccountScope
    interface Component {
        void inject(AccountFragment fragment);
    }
    //endregion
}
