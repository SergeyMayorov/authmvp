package ru.skill_branch.mayorov.mvpauth.mvp.views;


public interface IRootView extends IView{
    void showMessage(String message);
    void showError(Throwable e);

    void showLoad();
    void hideLoad();
}
