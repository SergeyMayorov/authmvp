package ru.skill_branch.mayorov.mvpauth.mvp.presenters;

/**
 * Created by Sergey on 29.10.2016.
 */

public interface IProductPresenter {
    void clickOnPlus();

    void clickOnMinus();
}
