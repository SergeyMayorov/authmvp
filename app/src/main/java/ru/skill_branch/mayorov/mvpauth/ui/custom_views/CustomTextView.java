package ru.skill_branch.mayorov.mvpauth.ui.custom_views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;


public class CustomTextView extends TextView {
    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/PTBebasNeueBook.ttf");
        setTypeface(tf, 1);
    }
}

