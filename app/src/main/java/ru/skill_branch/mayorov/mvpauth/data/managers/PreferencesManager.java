package ru.skill_branch.mayorov.mvpauth.data.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


import ru.skill_branch.mayorov.mvpauth.App;

public class PreferencesManager {
    private static final String TOKEN_PREFERENCES_KEY = "TOKEN_PREFERENCES_KEY";
    private final SharedPreferences mSharedPreferences;

    public PreferencesManager(Context context){
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

//    public PreferencesManager(){
//        this.mSharedPreferences = App.getSharedPreferences();
//    }

    public void saveToken(String token){
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(TOKEN_PREFERENCES_KEY,token);
        editor.apply();
    }

    public String loadToken(){
        return mSharedPreferences.getString(TOKEN_PREFERENCES_KEY,null);
    }
}
