package ru.skill_branch.mayorov.mvpauth.mvp.presenters;

import java.util.List;

import javax.inject.Inject;

import dagger.Provides;
import ru.skill_branch.mayorov.mvpauth.data.storage.dto.ProductDto;
import ru.skill_branch.mayorov.mvpauth.di.DaggerService;
import ru.skill_branch.mayorov.mvpauth.di.scopes.CatalogScope;
import ru.skill_branch.mayorov.mvpauth.mvp.models.CatalogModel;
import ru.skill_branch.mayorov.mvpauth.mvp.views.ICatalogView;
import ru.skill_branch.mayorov.mvpauth.mvp.views.IRootView;
import ru.skill_branch.mayorov.mvpauth.ui.activities.RootActivity;

public class CatalogPresenter extends AbstractPresenter<ICatalogView> implements ICatalogPresenter {
    @Inject
    RootPresenter mRootPresenter;

    @Inject
    CatalogModel mCatalogModel;

    private List<ProductDto> mProductDtoList;


    public CatalogPresenter() {
        Component component = DaggerService.getComponent(Component.class);
        if(component == null){
            component = createDaggerComponent();
            DaggerService.registerComponent(Component.class,component);
        }
        component.inject(this);
    }

    @Override
    public void initView() {
        if (mProductDtoList == null) {
            mProductDtoList = mCatalogModel.getProductList();
        }

        if (getView() != null) {
            getView().showCatalogView(mProductDtoList);
        }

    }


    @Override
    public void clickOnBuyButton(int position) {
        if (getView() != null) {
            if (checkUserAuth()) {
                getRootView().showMessage("Товар " + mProductDtoList.get(position).getProductName() + " кспешно добавлен в корзину");
            } else {
              getView().showAuthScreen();
            }
        }
    }

    private IRootView getRootView(){
        return mRootPresenter.getView();
    }


    @Override
    public boolean checkUserAuth() {
        return mCatalogModel.isUserAuth();
    }


    //region ==================== DI ===========================

    private Component createDaggerComponent(){
        return DaggerCatalogPresenter_Component.builder()
                .component(DaggerService.getComponent(RootActivity.Component.class))
                .module(new Module())
                .build();
    }

    @dagger.Module
    public class Module{
        @Provides
        @CatalogScope
        CatalogModel provideCatalogModel(){
            return new CatalogModel();
        }
    }

    @dagger.Component(dependencies = RootActivity.Component.class,modules = Module.class)
    @CatalogScope
    interface Component{
        void inject(CatalogPresenter presenter);
    }
    //endregion
}
